FROM node:10.20.1

# Create app directory
WORKDIR /usr/src/app

COPY package.json /usr/src/app
RUN npm install

COPY . /usr/src/app

EXPOSE 3000

CMD ["npm", "start"]