# practice-docker-express

## Docker command
```bash
docker build --no-cache -t lertumpai/project_name . #build image
docker run  --name express -p 3000:3000 lertumpai/node-express #run image and create container
```

## Docker clear cache
```bash
docker system prune -a #ใช้ลบทุกอย่างใน system
docker builder prune #ลบ docker cache
```